package main

import (
	"fmt"
)

var console = Console{}

type ConfigureRequest struct {
	Conf Configuration
}

type Configuration struct {
	Values map[string]string
}

func (o *Configuration) GetValue(key string) string {
	return o.Values[key]
}

func (o *Configuration) GetValueOrDefault(key string, defaultValue string) string {
	value := o.Values[key]
	if value != "" {
		return value
	}
	return defaultValue
}

type OptionsRequest struct {
	Ops Options
}

type Options struct {
	Values map[string]string
}

func (o *Options) GetValue(key string) string {
	return o.Values[key]
}
func (o *Options) SetValue(key string, value string) {
	o.Values[key] = value
}

func (o *Options) GetValueOrDefault(key string, defaultValue string) string {
	value := o.Values[key]
	if value != "" {
		return value
	}
	return defaultValue
}

type Console struct {
}

func (c Console) Log(message string) {
	fmt.Printf("%s\n", message)
}

func (c Console) Logf(format string, a ...interface{}) {
	c.Log(fmt.Sprintf(format, a))
}
