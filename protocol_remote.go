package main

import "sync"

type ProtocolRemote struct {
	plugin ProtocolPlugin
	mutex  *sync.RWMutex
}

func NewProtocolRemote(plugin ProtocolPlugin) *ProtocolRemote {
	return &ProtocolRemote{
		plugin: plugin,
		mutex:  &sync.RWMutex{},
	}
}

func (fsr *ProtocolRemote) Configure(req ConfigureRequest, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.Configure(console, req.Conf)
}

func (fsr *ProtocolRemote) CreateService(options Options, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	if err := fsr.plugin.CreateService(console, options); err != nil {
		return err
	}
	*output = true
	return nil
}

func (fsr *ProtocolRemote) RemoveService(options Options, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	if err := fsr.plugin.RemoveService(console, options); err != nil {
		return err
	}
	*output = true
	return nil
}

func (fsr *ProtocolRemote) CreateShare(options Options, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.CreateShare(console, options)
}

func (fsr *ProtocolRemote) RemoveShare(options Options, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.RemoveShare(console, options)
}

func (fsr *ProtocolRemote) ListShares(options Options, output *[]Share) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	list, err := fsr.plugin.ListShares(console, options)
	*output = list
	return err
}

func (fsr *ProtocolRemote) Destroy(req string, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.Destroy(console)
}
