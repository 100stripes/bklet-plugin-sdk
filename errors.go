package main

import (
	"errors"
)

var (
	NotSupportedError = errors.New("Feature not supported")
)
