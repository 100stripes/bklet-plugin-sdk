package main

import "sync"

type StorageRemote struct {
	plugin StoragePlugin
	mutex  *sync.RWMutex
}

func NewStorageRemote(plugin StoragePlugin) *StorageRemote {
	return &StorageRemote{
		plugin: plugin,
		mutex:  &sync.RWMutex{},
	}
}

func (sr *StorageRemote) Usage(req string, output *string) error {
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	*output = sr.plugin.Usage()
	return nil
}

func (sr *StorageRemote) Configure(req ConfigureRequest, output *bool) error {
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	*output = true
	return sr.plugin.Configure(console, req.Conf)
}

func (sr *StorageRemote) Destroy(param string, output *bool) error {
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	*output = true
	return sr.plugin.Destroy(console)
}

func (sr *StorageRemote) CreateDevice(req OptionsRequest, output *string)  error {
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	o, err := sr.plugin.CreateDevice(console, req.Ops)
	if err!= nil{
		return err
	}
	*output = o
	return nil
}

func (sr *StorageRemote) ModifyDevice(req OptionsRequest, output *string) error{
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	o, err := sr.plugin.ModifyDevice(console, req.Ops)
	if err!= nil{
		return err
	}
	*output = o
	return nil
}

func (sr *StorageRemote) DeleteDevice(req OptionsRequest, output *string) error {
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	o, err := sr.plugin.DeleteDevice(console, req.Ops)
	if err!= nil{
		return err
	}
	*output = o
	return nil
}

func (sr *StorageRemote) AttachDevice(req OptionsRequest, output *string) error{
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	o, err := sr.plugin.AttachDevice(console, req.Ops)
	if err!= nil{
		return err
	}
	*output = o
	return nil
}

func (sr *StorageRemote) DetachDevice(req OptionsRequest, output *string) error{
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	o, err := sr.plugin.DetachDevice(console, req.Ops)
	if err!= nil{
		return err
	}
	*output = o
	return nil
}

func (sr *StorageRemote) InfoDevice(req OptionsRequest, output *string) error{
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	o, err := sr.plugin.InfoDevice(console, req.Ops)
	if err!= nil{
		return err
	}
	*output = o
	return nil
}

func (sr *StorageRemote) MonitorDevice(req OptionsRequest, output *string) error{
	sr.mutex.Lock()
	defer sr.mutex.Unlock()
	o, err := sr.plugin.MonitorDevice(console, req.Ops)
	if err!= nil{
		return err
	}
	*output = o
	return nil
}
