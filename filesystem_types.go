package main

type (
	MkfsRequest struct {
		Label    string
		Dataraid string
		Devices  []string
		Mount    string
	}
	MkVolRequest struct {
		Label   string
		Path    string
		VolName string
	}
	MountRequest struct {
		Device  string
		Mount   string
		Options string
	}
	RmVolRequest struct {
		Label   string
		Path    string
		VolName string
	}
	LsVolRequest struct {
		Label string
		Mount string
	}
	SnapshotRequest struct {
		Label, SrcMount, SrcVol, TargetVol string
		Readonly                           bool
	}
)
