package main

type ProtocolPlugin interface {
	Configure(console Console, conf Configuration) error

	CreateService(console Console, options Options) error
	RemoveService(console Console, options Options) error

	CreateShare(console Console, options Options) error
	ListShares(console Console, options Options) ([]Share, error)
	RemoveShare(console Console, options Options) error

	Destroy(console Console) error
}
