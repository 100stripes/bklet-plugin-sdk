package main

// Interface that any storage provider needs to implement in order to be able
// to provision a particular type of storage:
type StoragePlugin interface {
	// Returns human readable information about the different options that this plugin can manage
	Usage() string
	// Callback at start-up time to set-up your plugin before it becames fully operative
	Configure(console Console, conf Configuration) error
	// Destroy callback to clean-up your resources before the process is killed
	Destroy(console Console) error
	// Create a device that you can attach to a VM or native OS
	CreateDevice(console Console, ops Options) (string, error)
	// Expanding the storage space of a device or volume
	ModifyDevice(console Console, ops Options) (string, error)
	// Remove a device
	DeleteDevice(console Console, ops Options) (string, error)
	// Attach a device to your VM instance or native OS
	AttachDevice(console Console, ops Options) (string, error)
	// Detaching a device from your VM instance or native OS
	DetachDevice(console Console, ops Options) (string, error)
	// Returns a descriptive information about your volume or device like: size, type, encrypted, master key used,...
	InfoDevice(console Console, ops Options) (string, error)
	// Provides metrics and status checks that you can use to monitor your device
	MonitorDevice(console Console, ops Options) (string, error)

}
