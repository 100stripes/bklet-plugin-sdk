package main

import (
	"sync"
)

type FilesystemRemote struct {
	plugin FilesystemPlugin
	mutex  *sync.RWMutex
}

func NewFilesystemRemote(plugin FilesystemPlugin) *FilesystemRemote {
	return &FilesystemRemote{
		plugin: plugin,
		mutex:  &sync.RWMutex{},
	}
}

func (fsr *FilesystemRemote) Configure(req ConfigureRequest, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.Configure(req.Conf)
}

func (fsr *FilesystemRemote) Destroy(param string, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.Destroy()
}

func (fsr *FilesystemRemote) Mkfs(req MkfsRequest, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.Mkfs(req.Label, req.Dataraid, req.Devices, req.Mount)
}

func (fsr *FilesystemRemote) Lsfs(param string, output *[]string) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	list, err := fsr.plugin.Lsfs()
	if err != nil {
		return err
	}
	*output = list
	return nil
}

func (fsr *FilesystemRemote) Mount(req MountRequest, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	if err := fsr.plugin.Mount(req.Device, req.Mount, req.Options); err != nil {
		return err
	}
	*output = true
	return nil
}

func (fsr *FilesystemRemote) Umount(mount string, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	if err := fsr.plugin.Umount(mount); err != nil {
		return err
	}
	*output = true
	return nil
}

func (fsr *FilesystemRemote) MkVol(req MkVolRequest, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.MkVol(req.Label, req.Path, req.VolName)
}

func (fsr *FilesystemRemote) RmVol(req RmVolRequest, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.RmVol(req.Label, req.Path, req.VolName)
}

func (fsr *FilesystemRemote) LsVol(req LsVolRequest, output *[]string) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	list, err := fsr.plugin.LsVol(req.Label, req.Mount)
	if err != nil {
		return err
	}
	*output = list
	return nil
}

func (fsr *FilesystemRemote) Snapshot(req SnapshotRequest, output *bool) error {
	fsr.mutex.Lock()
	defer fsr.mutex.Unlock()
	*output = true
	return fsr.plugin.Snapshot(req.Label, req.SrcMount, req.SrcVol, req.TargetVol, req.Readonly)
}
