package main

type FilesystemPlugin interface {
	Configure(conf Configuration) error
	Destroy() error

	Mkfs(label string, dataraid string, devices []string, mount string) error
	Lsfs() ([]string, error)
	Mount(device string, mount string, options string) error
	Umount(mount string) error

	MkVol(label string, path string, volumeName string) error
	RmVol(label string, path string, volumeName string) error
	LsVol(label string, mount string) ([]string, error)

	Snapshot(label, srcMount, srcVol, targetVol string, readonly bool) error
}
