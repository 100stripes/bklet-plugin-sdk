package main

type (
	ShareRequest struct {
		Name    string
		Options map[string]string
	}

	Share struct {
		Name    string
		Path    string
		Service string
	}
)
