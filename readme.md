bklet-plugin-sdk
=======

This repo contains the library to develop third party bklet compatible
plugins.

Currently there are three types of plugins:

- filesystem
- protocol
- backends (no yet available)

## Naming conventions

For a plugin to be discovered by bklet, will need to follow the naming convention:

bklet-*plugin_type*-*plugin_name*


Following are examples of plugins:

- bklet-filesystem-btrfs

- bklet-filesystem-zfs

- bklet-filesystem-ext4

- bklet-protocol-nfs

- bklet-protocol-smb

- bklet-backend-ebs

- bklet-backend-googlecompute

- bklet-backend-s3

- bklet-backend-digitalocean

- bklet-backend-azure



ToDO
====

[ ] Support for *backend* plugins

[ ] Create a proper suite of tests

[ ] Code documentation