package main

import (
	"errors"
	"fmt"
	"net"
	"net/rpc"
	"os"
	"strconv"
	"strings"
	"sync"

	"github.com/Sirupsen/logrus"
)

type Manager struct {
	// For debugging purposes only, in real life it will be always true
	authenticated bool

	// For debugging purposes only, range of ports scanned while serving the plugin
	minPort, maxPort int64

	// Channel to receive stop requests
	stopCh chan bool

	// InFlight conns to handle graceful degradation
	waitGroup *sync.WaitGroup

	// Listener
	listener net.Listener
}

// Accept connections and spawn a goroutine to serve each one. Stop listening
// if anything is received on the stop channel
func (m *Manager) Start() {
	defer m.waitGroup.Done()
	for {
		select {
		case <-m.stopCh:
			logrus.Infof("Stop listening on %s", m.listener.Addr().String())
			m.listener.Close()
			return
		default:

		}
		// Announce our presence (TCP port) to the bklet server
		announcement := fmt.Sprintf("%s|%s|%s\n",
			BKLET_PLUGIN_VERSION_VALUE,
			m.listener.Addr().Network(),
			m.listener.Addr().String())

		fmt.Println(announcement)
		os.Stdout.Sync()

		conn, err := m.listener.Accept()
		if err != nil {
			logrus.Fatal("Error accepting connections!!")
		}
		m.waitGroup.Add(1)
		go rpc.ServeConn(conn)
	}
}

// Stop the plugin manager (tcp server) by closing the channel
func (m *Manager) Stop() {
	close(m.stopCh)
	m.waitGroup.Wait()
	defer m.listener.Close()
}

func RegisterFilesystem(plugin FilesystemPlugin) error {
	server, err := registerPlugin(NewFilesystemRemote(plugin))
	if err != nil {
		return err
	}
	server.Start()
	return nil
}

func RegisterProtocol(plugin ProtocolPlugin) error {
	server, err := registerPlugin(NewProtocolRemote(plugin))
	if err != nil {
		return err
	}
	server.Start()
	return nil
}

func RegisterStorage(plugin StoragePlugin) error {
	server, err := registerPlugin(NewStorageRemote(plugin))
	if err != nil {
		return err
	}
	server.Start()
	return nil
}

func registerPlugin(rcvr interface{}) (pluginManager *Manager, err error) {
	m := &Manager{
		stopCh:    make(chan bool),
		waitGroup: &sync.WaitGroup{},
	}

	// Publish the filesystem methods into the defaultServer
	rpc.Register(rcvr)

	// make sure only the bklet server has invoked this plugin
	if os.Getenv(BKLET_PLUGIN_CREDENTIALS_KEY) != BKLET_PLUGIN_CREDENTIALS_VALUE {
		logrus.Errorf("Plugins cannot be run directly")
		os.Exit(-1)
	}
	m.authenticated = true

	// Make sure plugin version is compatible with bklet server
	if os.Getenv(BKLET_PLUGIN_VERSION_KEY) != BKLET_PLUGIN_VERSION_VALUE {
		return nil, errors.New(
			fmt.Sprintf("Plugin version %s, is not compatible with server %s", os.Getenv(BKLET_PLUGIN_VERSION_KEY), BKLET_PLUGIN_VERSION_VALUE))
	}

	// Determine the range of allowed ports to bind the server to.
	ports := os.Getenv(BKLET_PLUGIN_RANGE_PORTS_KEY)
	if ports == "" {
		logrus.Warnf("%s has not been declared. Using default ports: [%d:%d]",
			BKLET_PLUGIN_RANGE_PORTS_KEY, BKLET_PLUGIN_RANGE_PORTS_DEFAULT_MIN_VALUE, BKLET_PLUGIN_RANGE_PORTS_DEFAULT_MAX_VALUE)
		m.minPort = BKLET_PLUGIN_RANGE_PORTS_DEFAULT_MIN_VALUE
		m.maxPort = BKLET_PLUGIN_RANGE_PORTS_DEFAULT_MAX_VALUE
	}
	minPort, maxPort, err := parsePorts(ports)
	if err != nil {
		return nil, err
	}
	m.minPort = minPort
	m.maxPort = maxPort

	listener, err := serverTCPListener(minPort, maxPort)
	if err != nil {
		return nil, err
	}

	// Wrap up the listener and start accepting connections
	m.listener = listener
	return m, nil
}

// Parse the ports in the form of: minPort:maxPort
func parsePorts(portValue string) (int64, int64, error) {
	ports := strings.Split(portValue, ":")
	if len(ports) != 2 {
		return -1, -1, errors.New("`BKLET_PLUGIN_RANGE_PORTS_KEY` value cannot be parsed due to a syntax error. Ensure the ports are separated by a colon(:)")
	}
	min, _ := strconv.ParseInt(ports[0], 10, 64)
	max, _ := strconv.ParseInt(ports[1], 10, 64)

	return min, max, nil
}

func serverTCPListener(minPort, maxPort int64) (net.Listener, error) {
	var address string
	for port := minPort; port <= maxPort; port++ {
		address = fmt.Sprintf("127.0.0.1:%d", port)
		listener, err := net.Listen("tcp", address)
		if err == nil {
			return listener, nil
		}
	}
	logrus.Errorf("Could not bind plugin TCP listener")
	return nil, errors.New("Could not bind plugin TCP listener")
}
